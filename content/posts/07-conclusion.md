+++
author = "Antoine"
title = "Vers une modélisation"
date = "2021-04-06"
description = "Et si le déploiement continu n'était qu'une modélisation technique de l'édition en tant que processus&nbsp;?"
bibfile = "data/bibliographie.json"
weight = 7
+++
Matérialiser un dispositif intellectuel prend diverses formes, l'édition en est une, d'abord via des procédés manuels, puis mécaniques et désormais numériques.
Manipuler un texte, faire circuler un document en cours d'écriture, concevoir un objet de publication, fabriquer puis produire un livre&nbsp;: les solutions techniques pour _éditer_ existent depuis plusieurs siècles, l'informatique est le prolongement de ces recherches.

Le déploiement continu, utilisé dans le domaine du livre, constitue une forme de détournement, de _hack_.
Pensé d'abord pour produire des programmes, des logiciels, des applications ou des sites web, il n'a pas été conçu pour fabriquer des livres.
Pratique déjà ancienne dans le domaine informatique, des logiciels comme Jenkins ([www.jenkins.io](https://www.jenkins.io)) ou des plateformes comme GitLab ([www.gitlab.com](https://www.gitlab.com)) ont facilité son appropriation.
Nous l'avons vu, des initiatives éditoriales s'emparent de ces outils pour publier des livres.
Un processus comme le déploiement continu n'est pas accessible sans une certaine maîtrise du numérique, pourquoi alors se diriger vers ce qui semble d'une grande complexité&nbsp;?
Avant d'éditer, pensez d'abord à ce qui vous permet d'éditer — pour reprendre et détourner les mots de Servanne Monjour {{< cite monjour_abrupt_2021 >}}&nbsp;: en _hackant_ le déploiement continu, des éditeurs et des éditrices questionnent leur façon de faire.

Les technologies de l'édition numérique ont une histoire {{< cite blanc_technologies_2018 >}}, dans laquelle le déploiement continu s'inscrit aujourd'hui, en se basant notamment sur le balisage sémantique, des convertisseurs de documents ou de formats, ou encore la gestion de versions.
Il ne s'agit pas d'une rupture mais d'une évolution elle aussi continue — logique&nbsp;? —, le prolongement de recherches en matière de fabrication du livre.
Écrire l'algorithme du déploiement continu consiste en la description des instructions pour les programmes&nbsp;: cet algorithme n'est rien d'autre que la formulation de ce qui n'était peut-être pas totalement formulé.
Le processus d'édition est déplié, certaines pratiques informelles sont rendues explicites.

S'agit-il ici d'une recherche absolue de l'automatisation, comme si le livre ne méritait plus que quelques lignes de code pour pouvoir devenir artefact&nbsp;?
Il est indéniable qu'il s'agit de l'un des objectifs de cette méthode informatique, mais appliquée à l'édition il y a matière à expérimentation plus qu'à industrialisation.
Si une certaine forme d'automatisation peut être mise en place, c'est à la condition d'une modélisation, le geste éditorial est alors déplacé dans une démarche de structuration, de compréhension et de description.
Les technologies ne sont pas neutres, nous devons noter que les différents services de déploiement continu utilisent des programmes libres et des infrastructures ouvertes et documentées, ce qui a une forte influence sur la façon de diffuser cette pratique, et de penser des _modèles_.
Le manque d'initiatives questionne l'adoption de ce mode de production, les quelques exemples présentés ici devraient être complétés par une recherche plus importante, et des analyses adéquates.

Plutôt qu'une reconfiguration de la littérature, le déploiement est une trace supplémentaire du sillage mêlé du numérique et du littéraire {{< cite audet_ecrire_2015 >}}.
Les règles et instructions des scripts du déploiement continu font partie du geste éditorial.
L'écriture numérique n'est pas une dématérialisation de l'inscription, la modélisation du déploiement continu est une formulation en directives du processus éditorial via des langages informatiques, mais aussi une éditorialisation {{< cite vitali_rosati_quest-ce_2016 >}}, une énonciation dans l'énonciation {{< cite souchier_formes_2007 >}}.
