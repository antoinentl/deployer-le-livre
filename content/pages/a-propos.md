+++
author = "Antoine"
title = "À propos"
date = "2021-02-25"
layout = "page"
+++
_Communication_ d'[Antoine Fauchié](https://www.quaternum.net) dans le cadre du colloque [Études du livre au XXI<sup>e</sup> siècle](https://projets.ex-situ.info/etudesdulivre21/) qui se déroule progressivement pendant les mois de mars à mai 2021.
Cette entreprise s'établit selon une recherche performative, la publication se fait à raison d'un nouvel épisode tous les jours ou tous les deux jours, entre le 10 et le 24 mars 2021.

Antoine Fauchié est doctorant au Département des littératures de langue française de l'Université de Montréal et responsable de projets à la Chaire de recherche du Canada sur les écritures numériques, son travail de recherche est dirigé par Marcello Vitali-Rosati et Michael Sinatra.
